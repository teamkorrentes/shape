package ru.teamkorrentes.shape;

public interface Shape {


    String getColor();
    double getPoint();
    double area();



}
