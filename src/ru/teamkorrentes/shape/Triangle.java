package ru.teamkorrentes.shape;

public class Triangle implements Shape {
    @Override
    public String getColor() {
        return null;
    }

    @Override
    public double getPoint() {
        return 0;
    }

    public static double a;
    public static double h;

    public Triangle() {
        this.a = 1;
        this.h = 1;
    }

    public Triangle(double a, double h) {
        this.a = a;
        this.h = h;
    }

    public double area(){
        return a*h*0.5;
    }
}
